$(function () {
    $(document).on("click", ".js-create", function (e) {
        e.preventDefault();
        $("#modal-create-user").modal("show")
    });
    $(document).on("click", ".js-change", function (e) {
        e.preventDefault();

        var url = $(this).attr("href");
        $.ajax({
            url: url,
            success: function (response) {
                if (response.success) {
                    var changeModal = $("#modal-change-user"),
                        changeModalTitle = changeModal.find(".modal-title"),
                        changeModalBody = changeModal.find(".modal-body"),
                        changeUserForm = changeModalBody.find("#changeUserForm");
                    changeModalTitle.text("Alterar " + response.user.name);
                    changeUserForm.attr("action", url);
                    $.each(response.user, function (i, attr) {
                        changeModalBody.find("input[name=" + i + "]").val(attr);
                    });
                    changeModal.modal("show");
                }
            },
            error: function (response) {
                alert("A requisição falhou.")
            }
        });

        return false;
    });
    $(document).on("click", ".js-remove", function (e) {
        e.preventDefault();
        if (confirm("Você tem certeza que deseja remover este usuário?"))
            $("#user_"+response.id).fadeIn();
            $.ajax({
                url: $(this).attr("href"),
                method: "post",
                data: {
                    _method: "DELETE",
                    _token: $("input[name=_token]").val()
                },
                success: function (response) {
                    if (!response.success) {
                        alert("Algo deu errado =/.");
                        $("#user_"+response.id).fadeOut();
                    }
                },
                error: function (response) {
                    alert("A requisição falhou.");
                    $("#user_"+response.id).fadeOut();
                }
            });

        return false;
    });
});
