$(function () {
    $(document).on('click', '.js-change', function (e) {
        e.preventDefault();

        var url = $(this).attr("href");
        $.ajax({
            url: url,
            success: function(response){
                if(response.success){
                    var changeModal = $("#modal-change-theme"),
                        changeModalTitle = changeModal.find(".modal-title"),
                        changeModalBody = changeModal.find(".modal-body"),
                        changeThemeForm = changeModalBody.find("#changeThemeForm");
                    changeModalTitle.text("Alterar "+response.theme.name);
                    changeThemeForm.attr("action", url);
                    $.each(response.theme, function(i, attr){
                        changeModalBody.find("input[name=" + i + "]").val(attr);
                    });
                    changeModal.modal('show');
                }
            },
            error: function (response) {
                alert("A requisição falhou.")
            }
        });

        return false;
    });
    $(document).on('click', '.js-remove', function (e) {
        e.preventDefault();
        if (confirm("Você tem certeza que deseja remover este tema?"))
            $.ajax({
                url: $(this).attr("href"),
                method: "post",
                data: {
                    _method: "DELETE"
                },
                success: function(response){
                    if(response.success){
                        var changeModal = $("#modal-change-theme"),
                            changeModalTitle = changeModal.find(".modal-title"),
                            changeModalBody = changeModal.find(".modal-body");
                        changeModalTitle.text("Alterar "+response.theme.name);
                        $.each(response.theme, function(i, attr){
                            changeModalBody.find("input[name=" + i + "]").val(attr);
                        });
                        changeModal.modal('show');
                    }
                },
                error: function (response) {
                    alert("A requisição falhou.")
                }
            });

        return false;
    });
});
