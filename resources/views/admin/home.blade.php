@extends('layouts.app')

@section('title', 'Painel de Controle')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Painel de Controle</div>

                    <div class="panel-body">
                        Seja bem vindo ao seu painel de controle =)<br>
                        O que você gostaria de alterar?
                        <ul>
                            <li><a href="{{route('indexUsers')}}">Usuários</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
