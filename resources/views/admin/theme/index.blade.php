@extends('layouts.app')

@section('title', 'Temas | Painel de Controle')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Temas</div>

                    <div class="panel-body">
                        Esses são os temas cadastrados<br>
                        Qual você gostaria de alterar?
                            <ul>
                                @foreach($themes as $theme)
                                    <li>
                                        <a href="{{route('getChangeTheme', $theme->id)}}">{{$theme->name}}</a>
                                        <a href="{{route('postChangeTheme', $theme->id)}}" class="js-change"><button>Alterar</button></a>
                                        <a href="{{route('removeTheme', $theme->id)}}" class="js-remove"><button>Remover</button></a>
                                    </li>
                                @endforeach
                            </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modal.admin.theme-change')
    @include('modal.admin.theme-create')
@endsection

@section('scripts')
    <script src="{{asset('js/admin-theme.js')}}"></script>
@endsection
