@extends('layouts.app')

@section('title', 'Painel de Controle')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Usuário {{$user->id}}: {{$user->name}}</div>

                    <div class="panel-body">
                        <form action="{{route('postChangeUser', $user->id)}}" method="post">
                            {{method_field("PATCH")}}
                            {{csrf_field()}}
                            <label for="name">Nome</label>
                            <input type="text" id="name" name="name" value="{{$user->name}}">
                            <br>
                            <label for="username">Username</label>
                            <input type="text" id="username" name="username" value="{{$user->username}}">
                            <br>
                            <label for="email">E-mail</label>
                            <input type="text" id="email" name="email" value="{{$user->email}}">
                            <br>
                            <button type="submit">Salvar</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
