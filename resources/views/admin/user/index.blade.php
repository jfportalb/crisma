@extends('layouts.app')

@section('title', 'Usuários')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Usuários</div>

                    <div class="panel-body">
                        <a href="" class="js-create"><button>Criar Usuário</button></a><br>
                        Esses são os usuários cadastrados<br>
                        Quem você gostaria de alterar?
                        @foreach($users as $tipo)
                            <ul>
                                @foreach($tipo as $user)
                                    <li id="user_{{$user->id}}">
                                        <a href="{{route('getChangeUser', $user->id)}}">{{$user->name}}</a>
                                        <a href="{{route('postChangeUser', $user->id)}}" class="js-change"><button>Alterar</button></a>
                                        <a href="{{route('removeUser', $user->id)}}" class="js-remove"><button>Remover</button></a>
                                    </li>
                                @endforeach
                            </ul>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('modal.admin.user-change')
    @include('modal.admin.user-create')
@endsection

@section('scripts')
    <script src="{{asset('js/admin-user.js')}}"></script>
@endsection
