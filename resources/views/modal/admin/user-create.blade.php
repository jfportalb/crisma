<div class="modal fade" id="modal-create-user" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Criar usuário</h4>
            </div>
            <div class="modal-body">
                <form id="createUserForm" action="{{route("createUser")}}" method="post">
                    {{method_field("PUT")}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" id="name" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input type="text" id="username" class="form-control" name="username">
                    </div>
                    <div class="form-group">
                        <label for="email">E-mail</label>
                        <input type="text" id="email" class="form-control" name="email">
                    </div>
                    <button type="submit">Criar</button>
                </form>
            </div>
        </div>
    </div>
</div>