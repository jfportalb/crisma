<div class="modal fade" id="modal-change-theme" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="center modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"></h4>
            </div>
            <div class="modal-body">
                <form id="changeThemeForm" method="post">
                    {{method_field("PATCH")}}
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" id="name" class="form-control" name="name">
                    </div>
                    <div class="form-group">
                        <label for="description">Descrição</label>
                        <textarea id="description" class="form-control" name="description"></textarea>
                    </div>
                    <button type="submit">Salvar</button>
                </form>
            </div>
        </div>
    </div>
</div>