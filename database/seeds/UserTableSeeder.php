<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::create([
            'name' => 'Administrador',
            'email' => 'admin@email.com',
            'username' => 'admin',
            'password' => bcrypt('123'),
            'access_level' => \App\User::ADMIN,
        ]);
    }
}
