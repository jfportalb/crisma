<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    const CRISMANDO = 0;
    const DIRIGENTE = 1;
    const ADMIN = 2;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'username', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function absence()
    {
        return $this->hasMany('App\Absence');
    }

    public function isDirigente()
    {
        return $this->access_level >= User::DIRIGENTE;
    }

    public function isAdmin()
    {
        return $this->access_level === User::ADMIN;
    }

    public function becomeDirigente()
    {
        $this->update([
            'access_level' => User::DIRIGENTE
        ]);
    }

    public function becomeAdmin()
    {
        $this->update([
            'access_level' => User::ADMIN
        ]);
    }

    public function accessName(){
        switch ($this->access_level){
            case self::ADMIN:
                return "Administrador";
            case self::DIRIGENTE:
                return "Dirigente";
        }
        return "Crismando";
    }
}
