<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    public function reunion()
    {
        return $this->hasMany('App\Reunion');
    }
}
