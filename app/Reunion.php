<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reunion extends Model
{
    public function theme()
    {
        return $this->belongsTo('App\Theme');
    }

    public function absences()
    {
        return $this->hasMany('App\Absence');
    }
}
