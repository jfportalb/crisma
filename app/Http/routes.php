<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::get('/', function () {
        return view('welcome');
    });

    // Authentication routes...
    Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('login', 'Auth\AuthController@authenticate');
    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@logout']);

    // Registration routes...
    Route::get('register', ['as' => 'register', 'uses' => 'Auth\AuthController@getRegister']);
    Route::post('register', 'Auth\AuthController@postRegister');

    // Password reset link request routes...
    Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');

    // Password reset routes...
    Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
    Route::post('password/reset', 'Auth\PasswordController@reset');

    Route::get('/home', ['as' => 'home', 'uses' => 'HomeController@index']);

    Route::group(['middleware' => 'admin', 'prefix' => 'backend'], function () {
        Route::get('/', ['as' => 'backend', 'uses' => 'HomeController@getBackend']);

        //Routes for the user's CRUD
        Route::get('users', ['as' => 'indexUsers', 'uses' => 'UserController@index']);
        Route::put('create_user', ['as' => 'createUser', 'uses' => 'UserController@store']);
        Route::get('user/{id}', ['as' => 'getChangeUser', 'uses' => 'UserController@edit']);
        Route::patch('user/{id}', ['as' => 'postChangeUser', 'uses' => 'UserController@update']);
        Route::delete('remove_user/{id}', ['as' => 'removeUser', 'uses' => 'UserController@destroy']);
        
        //Routes for the theme's CRUD
        Route::get('themes', ['as' => 'indexThemes', 'uses' => 'ThemeController@index']);
        Route::get('theme/{id}', ['as' => 'getChangeTheme', 'uses' => 'ThemeController@edit']);
        Route::patch('theme/{id}', ['as' => 'postChangeTheme', 'uses' => 'ThemeController@update']);
        Route::delete('remove_theme/{id}', ['as' => 'removeTheme', 'uses' => 'ThemeController@destroy']);
    });
});
