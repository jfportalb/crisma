<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PDO;

class DBRestartCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:restart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restart the application database based on config';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $this->call('db:drop');
        $this->call('db:start');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            //array('example', InputArgument::REQUIRED, 'An example argument.'),
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }

}